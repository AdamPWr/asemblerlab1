/*
	Dioda
*/

ORG 0
loop:	MOV P1,#10000000B    					
		LCALL delay		      
		MOV P1,#11000000B
		LCALL delay
		MOV P1,#11100000B 			
		LCALL delay	 
		MOV P1,#11110000B
		LCALL delay	
		MOV P1,#11111000B 			
		LCALL delay	 
		MOV P1,#11111100B
		LCALL delay	
		MOV P1,#11111110B 			
		LCALL delay	 
		MOV P1,#11111111B
		LCALL delay	 
		MOV P1,#11111111B	    							
		LCALL delay		
		SJMP loop			
delay:

		MOV A,#10		; 10 * 100 MS = 1S
		
delayN100:
			PUSH ACC 
			
			MOV A,#200
delay100:
			PUSH ACC	;2 cykle
			MOV A,#226	;2 cykle
			DJNZ ACC,$	;226 * 2 cykle = 452 cykle
			POP ACC		;2 cykle
			DJNZ ACC,delay100 ;2cykle
			
			POP ACC
			DJNZ ACC,delayN100	;n*100ms

		RET		
END

		RET		
END
